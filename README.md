# MyScalesApp
My Scales App is a Android app with the goal of helping a musician with their practice routine by providing selected exercises in a randomized order. Created a multi-page app that passes data between activities, saved data in user preferences, and using libraries to hold business logic.  Includes the ability to generate custom practice routines based on user inputs.

## Installation
Clone this repository and import into **Android Studio**
```bash
git clone https://sethclim@bitbucket.org/sethclim/scales_app_android.git
```
## Technologies Used

-**Java** -**Android** -**XML**

## Demo
![PianoScalesApp_screenOne_small.png](https://bitbucket.org/repo/6zxydn8/images/2881926403-PianoScalesApp_screenOne_small.png)
![PianoScalesApp_screenTwo.png](https://bitbucket.org/repo/6zxydn8/images/2455529899-PianoScalesApp_screenTwo.png)


## Usage
Select technique and scale options from the selection screen to create a customized practice.
Then in the practice screen step through the generated practice list in a randomized order.
Difficult scales can be favourited to return to later.

## Future
The addition of a voice control for the "Next Scale Button" for hands free operation.
A stats page with historical data on scales practiced, displayed through graphs.

## Key Project Lessons
* Passing Data Between Activities
* Seperation of concerns, with business logic contained in its own library

## Maintainers
This project is mantained by:
* [Seth Climenhaga](https://bitbucket.org/sethclim/)
