package com.example.myscalesapp;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import com.example.createpractice.ScaleGen;

public class MyActivity_Settings extends Activity {

    public void onCheckboxClicked2(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        switch (view.getId()) {
            case R.id.check_A:
                ScaleGen.customRoots[9] = checked;
                break;
            case R.id.check_sharpA:
                ScaleGen.customRoots[10] = checked;
                break;
            case R.id.check_B:
                ScaleGen.customRoots[11] = checked;
                break;
            case R.id.check_C:
                ScaleGen.customRoots[0] = checked;
                break;
            case R.id.check_sharpC:
                ScaleGen.customRoots[1] = checked;
                break;
            case R.id.check_D:
                ScaleGen.customRoots[2] = checked;
                break;
            case R.id.check_sharpD:
                ScaleGen.customRoots[3] = checked;
                break;
            case R.id.check_E:
                ScaleGen.customRoots[4] = checked;
                break;
            case R.id.check_F:
                ScaleGen.customRoots[5] = checked;
                break;
            case R.id.check_sharpF:
                ScaleGen.customRoots[6] = checked;
                break;
            case R.id.check_G:
                ScaleGen.customRoots[7] = checked;
                break;
            case R.id.check_sharpG:
                ScaleGen.customRoots[8] = checked;
                break;
        }
    }

    protected void setCheckMarks(){
        CheckBox aCheckBox = findViewById(R.id.check_A);
        aCheckBox.setChecked(ScaleGen.customRoots[9]);

        CheckBox asCheckBox = findViewById(R.id.check_sharpA);
        asCheckBox.setChecked(ScaleGen.customRoots[10]);

        CheckBox bCheckBox = findViewById(R.id.check_B);
        bCheckBox.setChecked(ScaleGen.customRoots[11]);

        CheckBox cCheckBox = findViewById(R.id.check_C);
        cCheckBox.setChecked(ScaleGen.customRoots[0]);

        CheckBox csCheckBox = findViewById(R.id.check_sharpC);
        csCheckBox.setChecked(ScaleGen.customRoots[1]);

        CheckBox dCheckBox = findViewById(R.id.check_D);
        dCheckBox.setChecked(ScaleGen.customRoots[2]);

        CheckBox dsCheckBox = findViewById(R.id.check_sharpD);
        dsCheckBox.setChecked(ScaleGen.customRoots[3]);

        CheckBox eCheckBox = findViewById(R.id.check_E);
        eCheckBox.setChecked(ScaleGen.customRoots[4]);

        CheckBox fCheckBox = findViewById(R.id.check_F);
        fCheckBox.setChecked(ScaleGen.customRoots[5]);

        CheckBox fsCheckBox = findViewById(R.id.check_sharpF);
        fsCheckBox.setChecked(ScaleGen.customRoots[6]);

        CheckBox gCheckBox = findViewById(R.id.check_G);
        gCheckBox.setChecked(ScaleGen.customRoots[7]);

        CheckBox gsCheckBox = findViewById(R.id.check_sharpG);
        gsCheckBox.setChecked(ScaleGen.customRoots[8]);
    }

    @Override
    public void onCreate ( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView ( R.layout.sub_menu );
        setCheckMarks();
        Button okayClose = findViewById(R.id.okay_close);
        okayClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                        finish();
            }
        });
    }
    protected void onSaveInstanceState ( Bundle outState ) {
       super.onSaveInstanceState ( outState );
    }

    @Override
    protected void onResume() {
        setCheckMarks();
        super.onResume();
    }

    @Override
    public void finish () {
        setResult ( RESULT_OK , new Intent ());
        super.finish ();
    }

}
