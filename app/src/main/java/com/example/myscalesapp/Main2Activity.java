package com.example.myscalesapp;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.util.ArrayList;

import com.example.createpractice.ScaleGen;

public class Main2Activity extends AppCompatActivity {

    final ScaleGen myScale = new ScaleGen();
    public void onCheckboxClicked(View view) {
        // Is the view now checked?
        boolean checked = ((CheckBox) view).isChecked();

        // Check which checkbox was clicked
        switch(view.getId()) {
            case R.id.checkMaj:
                ScaleGen.scaleOptions[0][1] = checked;
                break;
            case R.id.checkMinH:
                ScaleGen.scaleOptions[1][1] = checked;
                break;
            case R.id.checkMinM:
                ScaleGen.scaleOptions[2][1] = checked;
                break;
            case R.id.dim:
                ScaleGen.scaleOptions[3][1] = checked;
                break;
            case R.id.maj7:
                ScaleGen.scaleOptions[4][1]= checked;
                break;
            case R.id.checkScales:
                myScale.techOptions[0][1] = checked;
                break;
            case R.id.checkOct:
                myScale.techOptions[1][1] = checked;
                break;
            case R.id.checkArp:
                myScale.techOptions[2][1] = checked;
                break;
            case R.id.checkBRCH:
                myScale.techOptions[3][1] = checked;
                break;
            case R.id.checkSLCH:
                myScale.techOptions[4][1] = checked;
                break;
            case R.id.checkCM:
                myScale.techOptions[5][1] = checked;
                break;
        }
    }
    public Boolean checked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);


        Button btn2 = findViewById(R.id.getCombos);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //myScale.myClear();
                if(!checked){

                    for(Object[] rootOption : ScaleGen.rootOptions){
                        rootOption[1] = true;
                    }
                }
                else{
                    myScale.resetRoot();
                    myScale.applyCustomRoot();
                }

                myScale.combo();
                if ((myScale.practice.isEmpty())) {
                    Toast toast = Toast.makeText(Main2Activity.this,"Please select an option", Toast.LENGTH_SHORT);
                    toast.setGravity(Gravity.CENTER, 0, 500);
                    toast.show();
                } else {
                    Intent intent = new Intent(Main2Activity.this, MainActivity.class);
                    intent.putExtra("practice", myScale.practice);
                    //start the activity connect to the specified class
                    startActivity(intent);
                }
            }
        });

        Button btn3 = findViewById(R.id.custRoot);
        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent ( Main2Activity.this , MyActivity_Settings.class );
                startActivityForResult ( intent , 0 );
            }
        });

        Switch rootSwitch = findViewById(R.id.rootmodeSwitch);
        rootSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
            {
                checked = isChecked;
            }
        });

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void loadFav() {
        SharedPreferences sharedPreferences = getSharedPreferences("shared preferences", MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("task list", null);
        Type type = new TypeToken<ArrayList<String>>() {}.getType();
        ScaleGen.favStore = gson.fromJson(json, type);

        if (ScaleGen.favStore == null){
            //toast will go here
        }

        else{
            Log.d("practice", String.valueOf(ScaleGen.favStore));
            Intent intent = new Intent(Main2Activity.this, MainActivity.class);
            intent.putExtra("practice", ScaleGen.favStore);
            //start the activity connect to the specified class
            startActivity(intent);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.pracFav) {
            loadFav();
        }
        return super.onOptionsItemSelected(item);
    }
}