package com.example.createpractice;
import java.util.ArrayList;

public class ScaleGen {

    public static Object[][] rootOptions = {
            {"C ", false},
            {"C# ", false},
            {"D ", false},
            {"Eb ", false},
            {"E ", false},
            {"F ", false},
            {"F# ", false},
            {"G ", false},
            {"Ab ", false},
            {"A ", false},
            {"Bb ", false},
            {"B ", false},
    };

    public static Object[][] scaleOptions = {
            {"Major ", false, true},
            {"Mel. Minor ", false, true},
            {"Har. Minor ", false, true},
            {"Dim ", false, false},
            {"Major 7 ", false, false},
    };


    public Object[][] techOptions = {
            {"Scales", false},
            {"Octaves", false},
            {"Arpeggio", false},
            {"Broken Chords", false},
            {"Solid Chords", false},
            {"Contrary Motion", false},
    };

    public ArrayList<String> practice = new ArrayList<String>(10);

    public static ArrayList<String> favStore = new ArrayList<String>(10);

    public static boolean[] customRoots = {false, false, false, false, false, false, false, false, false, false, false, false};

    public void combo(){
        //clearing results
        practice.clear();
        for (Object[] rootOption : rootOptions) {

            if ((boolean) rootOption[1]) {

                for (Object[] scaleOption : scaleOptions) {
                    if ((boolean) scaleOption[1] && (boolean) scaleOption[2]) {
                        for (Object[] techOptions : techOptions) {

                            if ((boolean) techOptions[1]) {
                                String temp = (String)rootOption[0] + scaleOption[0] + techOptions[0];
                                practice.add(temp);
                            }
                        }
                    }
                    else if ((boolean) scaleOption[1]){
                        for (int i = 5; i < techOptions.length; i++) {

                            if ((boolean) techOptions[i][1]) {
                                String temp = rootOption[0] + " " + scaleOption[0] + " " + techOptions[i][0];
                                practice.add(temp);
                            }
                        }
                    }
                }
            }
        }
    }


    public void resetRoot(){
        for(Object[] rootOption : rootOptions){
            rootOption[1] = false;
        }
    }

    public void applyCustomRoot(){
        for(int i = 0; i < rootOptions.length; i++){
            rootOptions[i][1] = customRoots[i];
        }
    }
}